import * as actionTypes from './actionTypes'

const apiBaseURI = 'https://crypticker-api.now.sh'
// const apiBaseURI = 'http://localhost:3001'

export const removeTicker = ticker => ({
  type: actionTypes.REMOVE_TICKER,
  ticker
})
export const moveUpTicker = ticker => ({
  type: actionTypes.MOVE_UP_TICKER,
  ticker
})
export const moveDownTicker = ticker => ({
  type: actionTypes.MOVE_DOWN_TICKER,
  ticker
})
export const activateEditMode = ticker => ({
  type: actionTypes.ACTIVATE_EDIT_MODE
})
export const deactivateEditMode = () => ({
  type: actionTypes.DEACTIVATE_EDIT_MODE
})
export const selectTickerToAdd = ticker => ({
  type: actionTypes.SELECT_TICKER_TO_ADD,
  ticker
})
export const addTicker = () => ({
  type: actionTypes.ADD_TICKER
})
export const addTickers = tickersIds => ({
  type: actionTypes.ADD_TICKERS,
  tickersIds
})
const requestTickers = () => ({
  type: actionTypes.REQUEST_TICKERS
})
const receiveTickers = tickers => ({
  type: actionTypes.RECEIVE_TICKERS,
  date: new Date().toISOString(),
  tickers
})
export const deactivateEditModeAndRefresh = selectedTickers => dispatch => {
  dispatch(deactivateEditMode())
  setTimeout(() => dispatch(refresh(selectedTickers)), 0)
}
const fetchTickers = (dispatch, selectedTickers) => {
  return fetch(
    `${apiBaseURI}/tickersValues?tickers=` +
      selectedTickers.map(ticker => ticker.id).join(',')
  )
    .then(res => res.json())
    .then(tickersValues => {
      const tickers = tickersValues.map(tickerValue => {
        const ticker = selectedTickers.find(
          ticker => ticker.id === tickerValue.id
        )
        return {
          ...ticker,
          value: tickerValue.value
        }
      })
      return tickers
    })
    .catch(err => {
      dispatch(showErrorAndHide('Error while fetching tickers.'))
      return []
    })
}
export const periodicRefresh = getSelectedTickers => dispatch => {
  fetchTickers(dispatch, getSelectedTickers()).then(tickers => {
    dispatch(receiveTickers(tickers))
    setTimeout(() => dispatch(periodicRefresh(getSelectedTickers)), 60000)
  })
}
export const refresh = selectedTickers => dispatch => {
  dispatch(requestTickers())
  return fetchTickers(dispatch, selectedTickers).then(tickers =>
    dispatch(receiveTickers(tickers))
  )
}
const requestCurrenciesAndAvailableTickers = () => ({
  type: actionTypes.REQUEST_CURRENCIES_AND_AVAILABLE_TICKERS
})
const receiveCurrenciesAndAvailableTickers = ({ currencies, tickers }) => ({
  type: actionTypes.RECEIVE_CURRENCIES_AND_AVAILABLE_TICKERS,
  currencies,
  tickers
})
export const getCurrenciesAndTickers = () => dispatch => {
  dispatch(requestCurrenciesAndAvailableTickers())

  const fetchCurrencies = fetch(`${apiBaseURI}/currencies`).then(res =>
    res.json()
  )

  const fetchAvailableTickers = fetch(
    `${apiBaseURI}/availableTickers`
  ).then(res => res.json())

  return Promise.all([fetchCurrencies, fetchAvailableTickers])
    .then(([currencies, tickers]) => {
      dispatch(receiveCurrenciesAndAvailableTickers({ currencies, tickers }))

      const defaultTickers = ['XXBTZUSD', 'XETHZUSD', 'XETHXXBT']
      dispatch(addTickers(defaultTickers))
      dispatch(
        refresh(
          defaultTickers.map(tickerId =>
            tickers.find(ticker => ticker.id === tickerId)
          )
        )
      )
    })
    .catch(err => {
      dispatch(
        showErrorAndHide(
          'Error while fetching currencies and tickers data. Please check your internet connection then reload the app.'
        )
      )
      return []
    })
}
export const openAboutDialog = () => ({
  type: actionTypes.OPEN_ABOUT_DIALOG
})
export const closeAboutDialog = () => ({
  type: actionTypes.CLOSE_ABOUT_DIALOG
})
export const hideError = () => ({
  type: actionTypes.HIDE_ERROR
})
export const showError = errorMessage => ({
  type: actionTypes.SHOW_ERROR,
  errorMessage
})
const showErrorAndHide = errorMessage => dispatch => {
  setTimeout(() => dispatch(hideError()), 10000)
  dispatch(showError(errorMessage))
}
