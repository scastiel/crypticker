export default [
  { id: 'XETH', symbol: 'ETH' },
  { id: 'XXBT', symbol: 'BTC' },
  { id: 'ZEUR', symbol: 'EUR' },
  { id: 'ZUSD', symbol: 'USD' }
]
