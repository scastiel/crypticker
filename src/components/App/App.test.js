import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import configureStore from '../../store/configureStore'
import currenciesApiMock from './currenciesApiMock'
import availableTickersApiMock from './availableTickersApiMock'
import tickersValuesApiMock from './tickersValuesApiMock'

window.fetch = jest.fn().mockImplementation(async url => ({
  async json() {
    if (url.match(/\/currencies/)) {
      return currenciesApiMock
    } else if (url.match(/\/availableTickers/)) {
      return availableTickersApiMock
    } else if (url.match(/\/tickersValues/)) {
      return tickersValuesApiMock
    } else {
      return {}
    }
  }
}))

window.localStorage = {
  store: {},
  getItem(key) {
    if (this.store[key] === undefined) {
      return null
    } else {
      return this.store[key]
    }
  },
  setItem(key, value) {
    this.store[key] = String(value)
  },
  clear() {
    this.store = {}
  }
}

it('renders without crashing', () => {
  const store = configureStore()
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    div
  )
})

it('renders without crashing with things in state', () => {
  const currencies = [
    { id: 'BTC', symbol: 'Ƀ' },
    { id: 'EUR', symbol: '€' },
    { id: 'USD', symbol: '$' },
    { id: 'ETH', symbol: 'E' }
  ]
  const tickers = [
    {
      id: 'BTCEUR',
      currFrom: 'BTC',
      currTo: 'EUR',
      value: 950.0
    },
    {
      id: 'BTCUSD',
      currFrom: 'BTC',
      currTo: 'USD',
      value: 1000.0
    },
    {
      id: 'ETHBTC',
      currFrom: 'ETH',
      currTo: 'BTC',
      value: 0.00115
    },
    {
      id: 'ETHEUR',
      currFrom: 'ETH',
      currTo: 'EUR',
      value: 10.5
    },
    {
      id: 'ETHUSD',
      currFrom: 'ETH',
      currTo: 'USD',
      value: 11
    }
  ]
  const selectedTickers = tickers.slice(0, 3).map(t => t.id)
  const selectedTickerToAdd = tickers[3].id
  const initialState = {
    currencies,
    tickers,
    selectedTickers,
    selectedTickerToAdd
  }
  const store = configureStore(initialState)
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    div
  )
})
