import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as tickerActions from '../../actions/tickerActions'
import TickerApp from '../TickerApp/TickerApp'

class App extends Component {
  render() {
    return <TickerApp {...this.props} />
  }
}

function mapStateToProps(state) {
  return { ...state }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(tickerActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
