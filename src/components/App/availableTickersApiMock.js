export default [
  { id: 'XETHXXBT', currFrom: 'XETH', currTo: 'XXBT', value: null },
  { id: 'XETHZEUR', currFrom: 'XETH', currTo: 'ZEUR', value: null },
  { id: 'XETHZUSD', currFrom: 'XETH', currTo: 'ZUSD', value: null },
  { id: 'XXBTZEUR', currFrom: 'XXBT', currTo: 'ZEUR', value: null },
  { id: 'XXBTZUSD', currFrom: 'XXBT', currTo: 'ZUSD', value: null }
]
