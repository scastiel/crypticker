import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import muiThemeable from 'material-ui/styles/muiThemeable'

function AboutDialog({ closeAboutDialog, muiTheme }) {
  const handleClose = () => {
    if (closeAboutDialog) {
      closeAboutDialog()
    }
  }
  const makeLink = (label, href) =>
    <a
      href={href}
      style={{ color: muiTheme.palette.primary1Color }}
      target="_blank"
      rel="noopener noreferrer"
    >
      {label}
    </a>
  return (
    <Dialog
      title="About Crypticker"
      actions={
        <FlatButton
          className="close-button"
          label="Close"
          primary={true}
          onTouchTap={handleClose}
        />
      }
      modal={false}
      open={true}
      autoScrollBodyContent={true}
      onRequestClose={handleClose}
      contentStyle={{ width: '95%' }}
      bodyStyle={{ paddingTop: '24px' }}
    >
      <p>
        {makeLink('Crypticker', '//scastiel.gitlab.io/crypticker')} is open
        source and distributed under the terms of the{' '}
        {makeLink(
          'GNU GPL v3',
          'https://www.gnu.org/licenses/gpl-3.0.en.html'
        )}{' '}
        licence.
      </p>
      <p>
        It's built with {makeLink('React', '//facebook.github.io/react/')},{' '}
        {makeLink(
          'create-react-app',
          '//github.com/facebookincubator/create-react-app'
        )}{' '}
        and {makeLink('Material-UI', '//www.material-ui.com')}. The source code
        is
        hosted on{' '}
        {makeLink('GitLab.com', 'https://gitlab.com/scastiel/crypticker')}, the
        web app runs on GitLab Pages, and the API on{' '}
        {makeLink('now.sh', 'https://zeit.co/now')}.
      </p>
      <p>
        Data is provided by{' '}
        {makeLink('Kraken', '//www.kraken.com/')} API.
      </p>
      <p>
        {makeLink('Find us on Twitter', '//twitter.com/crypticker_io')}!
      </p>
      <p style={{ fontSize: 'small' }}>
        Written and maintained by{' '}
        {makeLink('Sébastien Castiel', '//twitter.com/scastiel')}.
      </p>
    </Dialog>
  )
}

AboutDialog.propTypes = {
  closeAboutDialog: PropTypes.func
}

export default muiThemeable()(AboutDialog)
