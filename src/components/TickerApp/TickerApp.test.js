import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { expect } from 'chai'
import ReactTestUtils from 'react-dom/test-utils'

const mockComponent = Component
jest.mock(
  '../TickerListView/TickerListView',
  () =>
    class TickerListView extends mockComponent {
      moveUp(event) {
        event.preventDefault()
        this.props.moveUpTicker &&
          this.props.moveUpTicker(this.props.tickers[1])
      }
      moveDown(event) {
        event.preventDefault()
        this.props.moveDownTicker &&
          this.props.moveDownTicker(this.props.tickers[1])
      }
      remove(event) {
        event.preventDefault()
        this.props.removeTicker &&
          this.props.removeTicker(this.props.tickers[1])
      }
      render() {
        return (
          <div>
            <span className="edit-mode">
              {this.props.editMode ? 'on' : 'off'}
            </span>
            <ul>
              {this.props.tickers.map((ticker, index) =>
                <li key={index} className="ticker">{ticker.id}</li>
              )}
            </ul>
            <button className="move-up-button" onClick={e => this.moveUp(e)}>
              Up
            </button>
            <button
              className="move-down-button"
              onClick={e => this.moveDown(e)}
            >
              Down
            </button>
            <button className="remove-button" onClick={e => this.remove(e)}>
              Remove
            </button>
          </div>
        )
      }
    }
)
jest.mock(
  '../AddTickerForm/AddTickerForm',
  () =>
    class AddTickerFormMock extends mockComponent {
      onChange(event) {
        event.preventDefault()
        const tickerId = event.target.value
        const ticker = this.props.tickers.find(ticker => ticker.id === tickerId)
        this.props.selectTicker && this.props.selectTicker(ticker)
      }
      addTicker(event) {
        event.preventDefault()
        this.props.addTicker && this.props.addTicker()
      }
      render() {
        return (
          <div className="add-ticker-form">
            <ul className="available-tickers">
              {this.props.tickers.map(ticker =>
                <li key={ticker.id}>{ticker.id}</li>
              )}
            </ul>
            <input
              type="text"
              className="ticker-to-add-input"
              value={this.props.selectedTicker && this.props.selectedTicker.id}
              onChange={e => this.onChange(e)}
            />
            <button
              className="add-ticker-button"
              onClick={event => this.addTicker(event)}
            />
          </div>
        )
      }
    }
)
const TickerApp = require('./TickerApp').default

const currencies = [
  { id: 'BTC', symbol: 'Ƀ' },
  { id: 'EUR', symbol: '€' },
  { id: 'USD', symbol: '$' },
  { id: 'ETH', symbol: 'E' }
]
const tickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  },
  {
    id: 'BTCUSD',
    currFrom: 'BTC',
    currTo: 'USD',
    value: 1000.0
  },
  {
    id: 'ETHBTC',
    currFrom: 'ETH',
    currTo: 'BTC',
    value: 0.00115
  },
  {
    id: 'ETHEUR',
    currFrom: 'ETH',
    currTo: 'EUR',
    value: 10.5
  },
  {
    id: 'ETHUSD',
    currFrom: 'ETH',
    currTo: 'USD',
    value: 11
  }
]
const selectedTickers = tickers.slice(0, 3).map(t => t.id)
const selectedTickerToAdd = tickers[3].id

it('should display selected tickers', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
    />,
    div
  )
  const tickerElements = div.querySelectorAll('li.ticker')
  expect(Array.from(tickerElements).map(e => e.innerHTML)).to.deep.equal(
    selectedTickers
  )
})

it('should activate edit mode in tickers list if edit mode is on', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  expect(div.querySelector('.edit-mode').innerHTML).to.equal('on')
})

it('should not activate edit mode in tickers list if edit mode is off', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      selectedTickerToAdd={selectedTickerToAdd}
      editMode={false}
    />,
    div
  )
  expect(div.querySelector('.edit-mode').innerHTML).to.equal('off')
})

it('should respond to action buttons', () => {
  const div = document.createElement('div')
  const moveUpTicker = jest.fn()
  const moveDownTicker = jest.fn()
  const removeTicker = jest.fn()
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      moveUpTicker={moveUpTicker}
      moveDownTicker={moveDownTicker}
      removeTicker={removeTicker}
    />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.move-up-button'))
  expect(moveUpTicker.mock.calls).to.have.lengthOf(1)
  expect(moveUpTicker.mock.calls[0][0]).to.equal(tickers[1])
  ReactTestUtils.Simulate.click(div.querySelector('.move-down-button'))
  expect(moveDownTicker.mock.calls).to.have.lengthOf(1)
  expect(moveDownTicker.mock.calls[0][0]).to.equal(tickers[1])
  ReactTestUtils.Simulate.click(div.querySelector('.remove-button'))
  expect(removeTicker.mock.calls).to.have.lengthOf(1)
  expect(removeTicker.mock.calls[0][0]).to.equal(tickers[1])
})

it('should not crash if no functions passed for action buttons', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
    />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.move-up-button'))
  ReactTestUtils.Simulate.click(div.querySelector('.move-down-button'))
  ReactTestUtils.Simulate.click(div.querySelector('.remove-button'))
})

it('should show edit button but not add form if edit mode is off and respond to click', () => {
  const activateEditMode = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={false}
      activateEditMode={activateEditMode}
    />,
    div
  )
  const menuButton = div.querySelector('.icon-menu')
  expect(menuButton).to.exist
  ReactTestUtils.Simulate.click(menuButton)
  // const editButton = div.querySelector('.activate-edit-mode-button')
  // expect(editButton).to.exist
  // expect(div.querySelector('.deactivate-edit-mode-button')).not.to.exist
  // expect(div.querySelector('.add-ticker-form')).not.to.exist
  // ReactTestUtils.Simulate.click(editButton)
  // expect(activateEditMode.mock.calls).to.have.lengthOf(1)
})

it('should not crash if no function passed for activate edit mode', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={false}
    />,
    div
  )
  const menuButton = div.querySelector('.icon-menu')
  expect(menuButton).to.exist
  // ReactTestUtils.Simulate.touchTap(menuButton)
  // const editButton = document.querySelector('.activate-edit-mode-button')
  // expect(editButton).to.exist
  // ReactTestUtils.Simulate.click(editButton)
})

it('should show end button and add form if edit mode is on and respond to click', () => {
  const deactivateEditModeAndRefresh = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
      deactivateEditModeAndRefresh={deactivateEditModeAndRefresh}
    />,
    div
  )
  const endButton = div.querySelector('.deactivate-edit-mode-button')
  expect(endButton).to.exist
  expect(div.querySelector('.activate-edit-mode-button')).not.to.exist
  expect(div.querySelector('.add-ticker-form')).to.exist
  ReactTestUtils.Simulate.click(endButton)
  expect(deactivateEditModeAndRefresh.mock.calls).to.have.lengthOf(1)
})

it('should not crash if no function passed for deactivate edit mode', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  const endButton = div.querySelector('.deactivate-edit-mode-button')
  ReactTestUtils.Simulate.click(
    div.querySelector('.deactivate-edit-mode-button')
  )
})

it('should list unselected tickers in add ticker select', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  const availableTickersList = div.querySelector('.available-tickers')
  const availableTickers = Array.from(
    availableTickersList.querySelectorAll('li')
  ).map(li => li.innerHTML)
  expect(availableTickers).to.deep.equal(tickers.slice(3).map(t => t.id))
})

it('should respond to selected ticker to add change', () => {
  const selectTickerToAdd = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
      selectTickerToAdd={selectTickerToAdd}
    />,
    div
  )
  ReactTestUtils.Simulate.change(div.querySelector('.ticker-to-add-input'), {
    target: { value: tickers[4].id }
  })
  expect(selectTickerToAdd.mock.calls).to.have.lengthOf(1)
  expect(selectTickerToAdd.mock.calls[0][0]).to.equal(tickers[4])
})

it('should not crash if no function passed for selected ticker to add change', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  ReactTestUtils.Simulate.change(div.querySelector('.ticker-to-add-input'), {
    target: { value: tickers[4].id }
  })
})

it('should respond to add ticker', () => {
  const addTicker = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
      addTicker={addTicker}
    />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.add-ticker-button'))
  expect(addTicker.mock.calls).to.have.lengthOf(1)
})

it('should not crash if no function passed for add ticker', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.add-ticker-button'))
})

it('should show a refresh button and call refresh function when clicked', () => {
  const refresh = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={false}
      refresh={refresh}
    />,
    div
  )
  const refreshButton = div.querySelector('.refresh-button')
  expect(refreshButton).to.exist
  expect(refreshButton.hasAttribute('disabled')).to.be.false
  expect(refreshButton.innerHTML).to.contain('Refresh')
  ReactTestUtils.Simulate.click(refreshButton)
  expect(refresh.mock.calls).to.have.lengthOf(1)
})

it('should disable refresh button when refreshing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      requestingTickers={true}
    />,
    div
  )
  expect(div.querySelector('.refresh-indicator')).to.exist
  const refreshButton = div.querySelector('.refresh-button')
  expect(refreshButton).to.exist
  expect(refreshButton.hasAttribute('disabled')).to.be.true
  expect(refreshButton.innerHTML).to.contain('Refreshing…')
})

it('should show not crash when trying to refresh if no refresh function passed', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={false}
    />,
    div
  )
  const refreshButton = div.querySelector('.refresh-button')
  expect(refreshButton).to.exist
  ReactTestUtils.Simulate.click(refreshButton)
})

it('should not show refresh button if edit mode is on', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      editMode={true}
    />,
    div
  )
  expect(div.querySelector('.refresh-button')).not.to.exist
})

it('should not show last updated date if not provided', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
    />,
    div
  )
  expect(div.querySelector('.last-updated')).not.to.exist
})

it('should show last updated date if provided', () => {
  const div = document.createElement('div')
  const lastUpdatedDate = new Date()
  ReactDOM.render(
    <TickerApp
      selectedTickers={selectedTickers}
      currencies={currencies}
      tickers={tickers}
      lastUpdated={lastUpdatedDate.toISOString()}
    />,
    div
  )
  expect(div.querySelector('.last-updated')).to.exist
  expect(div.querySelector('.last-updated').innerHTML).to.contain(
    lastUpdatedDate.toLocaleString()
  )
})
