import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TickerListView from '../TickerListView/TickerListView'
import AddTickerForm from '../AddTickerForm/AddTickerForm'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh'
import SaveIcon from 'material-ui/svg-icons/content/save'
import { white, lightBlue500, lightBlue700 } from 'material-ui/styles/colors'
import IconButton from 'material-ui/IconButton'
import AppBar from 'material-ui/AppBar'
import RefreshIndicator from 'material-ui/RefreshIndicator'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import AboutDialog from '../AboutDialog/AboutDialog'
import Snackbar from 'material-ui/Snackbar'

import 'roboto-fontface'
import './TickerApp.css'

import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

export default class TickerApp extends Component {
  static propTypes = {
    tickers: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        currFrom: PropTypes.string.isRequired,
        currTo: PropTypes.string.isRequired
      })
    ).isRequired,
    currencies: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        symbol: PropTypes.string.isRequired
      })
    ).isRequired,
    selectedTickers: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedTickerToAdd: PropTypes.string,
    editMode: PropTypes.bool,
    removeTicker: PropTypes.func,
    moveUpTicker: PropTypes.func,
    moveDownTicker: PropTypes.func,
    activateEditMode: PropTypes.func,
    deactivateEditModeAndRefresh: PropTypes.func,
    selectTickerToAdd: PropTypes.func,
    addTicker: PropTypes.func,
    refresh: PropTypes.func,
    aboutDialogOpen: PropTypes.bool,
    openAboutDialog: PropTypes.func,
    closeAboutDialog: PropTypes.func,
    lastUpdated: PropTypes.string,
    errorDisplayed: PropTypes.bool,
    errorMessage: PropTypes.string,
    hideError: PropTypes.func
  }
  getTicker(tickerId) {
    return this.props.tickers.find(ticker => ticker.id === tickerId)
  }
  moveUpTicker(ticker) {
    if (this.props.moveUpTicker) {
      this.props.moveUpTicker(ticker)
    }
  }
  moveDownTicker(ticker) {
    if (this.props.moveDownTicker) {
      this.props.moveDownTicker(ticker)
    }
  }
  removeTicker(ticker) {
    if (this.props.removeTicker) {
      this.props.removeTicker(ticker)
    }
  }
  onDeactivateEditModeClick() {
    if (this.props.deactivateEditModeAndRefresh) {
      const selectedTickers = this.props.selectedTickers.map(tickerId =>
        this.props.tickers.find(ticker => ticker.id === tickerId)
      )
      this.props.deactivateEditModeAndRefresh(selectedTickers)
    }
  }
  onActivateEditModeClick() {
    if (this.props.activateEditMode) {
      this.props.activateEditMode()
    }
  }
  onTickerSelected(ticker) {
    if (this.props.selectTickerToAdd) {
      this.props.selectTickerToAdd(ticker)
    }
  }
  onTickerAdded() {
    if (this.props.addTicker) {
      this.props.addTicker()
    }
  }
  onRefreshButtonClicker(event) {
    if (this.props.refresh) {
      const selectedTickers = this.props.selectedTickers.map(tickerId =>
        this.props.tickers.find(ticker => ticker.id === tickerId)
      )
      this.props.refresh(selectedTickers)
    }
  }
  onAboutMenuClick() {
    if (this.props.openAboutDialog) {
      this.props.openAboutDialog()
    }
  }
  onErrorMessageClicked() {
    if (this.props.hideError) {
      this.props.hideError()
    }
  }
  render() {
    // lightBlue900
    const theme = getMuiTheme({
      ...lightBaseTheme,
      palette: {
        primary1Color: lightBlue500,
        pickerHeaderColor: lightBlue500,
        primary2Color: lightBlue700
      }
    })
    return (
      <MuiThemeProvider muiTheme={theme}>
        <div>
          <AppBar
            style={{ position: 'fixed', top: 0 }}
            className="header"
            title="Crypticker"
            titleStyle={{ textAlign: 'center' }}
            iconElementLeft={
              <div style={{ width: '48px' }}>
                {this.props.editMode
                  ? <IconButton
                      className="deactivate-edit-mode-button"
                      onClick={() => this.onDeactivateEditModeClick()}
                    >
                      <span className="sr-only">Save</span>
                      <SaveIcon color={white} />
                    </IconButton>
                  : <IconButton
                      className="refresh-button"
                      onClick={() => this.onRefreshButtonClicker()}
                      disabled={this.props.requestingTickers}
                    >
                      <span className="sr-only">
                        {this.props.requestingTickers
                          ? 'Refreshing…'
                          : 'Refresh'}
                      </span>
                      <RefreshIcon color={white} />
                    </IconButton>}
              </div>
            }
            iconElementRight={
              <div style={{ width: '48px' }}>
                {this.props.editMode ||
                  <IconMenu
                    className="icon-menu"
                    iconButtonElement={
                      <IconButton><MoreVertIcon color={white} /></IconButton>
                    }
                    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                  >
                    <MenuItem
                      className="activate-edit-mode-button"
                      primaryText="Edit"
                      onTouchTap={() => this.onActivateEditModeClick()}
                    />
                    <MenuItem
                      primaryText="About"
                      onTouchTap={() => this.onAboutMenuClick()}
                    />
                  </IconMenu>}
              </div>
            }
          />
          {this.props.requestingTickers &&
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                marginTop: '10px'
              }}
            >
              <RefreshIndicator
                className="refresh-indicator"
                style={{
                  display: 'block',
                  position: 'relative'
                }}
                size={40}
                left={10}
                top={0}
                status="loading"
              />
            </div>}
          <TickerListView
            tickers={this.props.selectedTickers.map(tickerId =>
              this.getTicker(tickerId)
            )}
            currencies={this.props.currencies}
            editMode={this.props.editMode}
            moveUpTicker={ticker => this.moveUpTicker(ticker)}
            moveDownTicker={ticker => this.moveDownTicker(ticker)}
            removeTicker={ticker => this.removeTicker(ticker)}
          />
          {!this.props.editMode &&
            this.props.lastUpdated &&
            <div
              className="last-updated"
              style={{
                color: theme.palette.accent3Color,
                fontSize: 'small',
                textAlign: 'center'
              }}
            >
              Last updated: {new Date(this.props.lastUpdated).toLocaleString()}
            </div>}
          {this.props.editMode &&
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <AddTickerForm
                tickers={this.props.tickers.filter(
                  ticker => !this.props.selectedTickers.includes(ticker.id)
                )}
                currencies={this.props.currencies}
                selectedTicker={this.getTicker(this.props.selectedTickerToAdd)}
                selectTicker={ticker => this.onTickerSelected(ticker)}
                addTicker={() => this.onTickerAdded()}
              />
            </div>}
          {this.props.aboutDialogOpen &&
            <div>
              <AboutDialog closeAboutDialog={this.props.closeAboutDialog} />
            </div>}
          {this.props.errorDisplayed &&
            <Snackbar
              open={true}
              message={this.props.errorMessage}
              onRequestClose={() => this.onErrorMessageClicked()}
            />}
        </div>
      </MuiThemeProvider>
    )
  }
}
