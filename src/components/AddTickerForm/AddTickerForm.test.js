import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'
import { expect } from 'chai'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

const mockComponent = Component
jest.mock(
  '../TickerSelector/TickerSelector',
  () =>
    class TickerSelectorMock extends mockComponent {
      onChange (event) {
        event.preventDefault()
        const tickerId = event.target.value
        const ticker = this.props.tickers.find(ticker => ticker.id === tickerId)
        this.props.selectTicker && this.props.selectTicker(ticker)
      }
      render () {
        return (
          <input
            type='text'
            value={
              this.props.selectedTicker ? this.props.selectedTicker.id : ''
            }
            onChange={e => this.onChange(e)}
          />
        )
      }
    }
)

const AddTickerForm = require('./AddTickerForm').default

const currencies = [
  { id: 'BTC', symbol: 'Ƀ' },
  { id: 'EUR', symbol: '€' },
  { id: 'USD', symbol: '$' }
]
const tickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  },
  {
    id: 'BTCUSD',
    currFrom: 'BTC',
    currTo: 'USD',
    value: 1000.0
  }
]

it('should display selected ticker', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <AddTickerForm
        currencies={currencies}
        tickers={tickers}
        selectedTicker={tickers[0]}
      />
    </MuiThemeProvider>,
    div
  )
  expect(div.querySelector('.add-ticker-button').disabled).to.be.false
  const input = div.querySelector('input')
  expect(input).to.exist
  expect(input.value).to.equal(tickers[0].id)
})

it('should disable add button if no available ticker', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <AddTickerForm
        currencies={currencies}
        tickers={[]}
        selectedTicker={null}
      />
    </MuiThemeProvider>,
    div
  )
  expect(div.querySelector('.add-ticker-button').disabled).to.be.true
})

it('should call selectTicker when a ticker is selected', () => {
  const selectTicker = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <AddTickerForm
        currencies={currencies}
        tickers={tickers}
        selectedTicker={tickers[0]}
        selectTicker={selectTicker}
      />
    </MuiThemeProvider>,
    div
  )
  const input = div.querySelector('input')
  expect(input).to.exist
  ReactTestUtils.Simulate.change(input, { target: { value: tickers[1].id } })
  expect(selectTicker.mock.calls).to.have.lengthOf(1)
  expect(selectTicker.mock.calls[0][0]).to.deep.equal(tickers[1])
})

it('should call addTicker when Add button is clicked', () => {
  const addTicker = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <AddTickerForm
        currencies={currencies}
        tickers={tickers}
        selectedTicker={tickers[0]}
        addTicker={addTicker}
      />
    </MuiThemeProvider>,
    div
  )
  const button = div.querySelector('button.add-ticker-button')
  expect(button).to.exist
  ReactTestUtils.Simulate.click(button)
  expect(addTicker.mock.calls).to.have.lengthOf(1)
})
