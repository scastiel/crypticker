import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TickerSelector from '../TickerSelector/TickerSelector'
import { Card, CardText, CardActions } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'

import './AddTickerForm.css'

const cardsStyle = {
  flexGrow: 1,
  flexShrink: 0,
  margin: '0 10px 12px',
  width: '100wh',
  minWidth: '300px',
  maxWidth: '400px'
}

export default class AddTickerForm extends Component {
  static propTypes = {
    tickers: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        currFrom: PropTypes.string.isRequired,
        currTo: PropTypes.string.isRequired
      })
    ).isRequired,
    currencies: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        symbol: PropTypes.string.isRequired
      })
    ).isRequired,
    selectedTicker: PropTypes.shape({
      id: PropTypes.string.isRequired,
      currFrom: PropTypes.string.isRequired,
      currTo: PropTypes.string.isRequired
    }),
    selectTicker: PropTypes.func,
    addTicker: PropTypes.func
  }
  onTickerSelected(ticker) {
    this.props.selectTicker && this.props.selectTicker(ticker)
  }
  onAddButtonClicked(event) {
    this.props.addTicker && this.props.addTicker()
  }
  render() {
    return (
      <Card className="add-ticker-form" style={cardsStyle}>
        <CardText style={{ paddingBottom: 0, marginBottom: '-10px' }}>
          <TickerSelector
            tickers={this.props.tickers}
            selectedTicker={this.props.selectedTicker}
            currencies={this.props.currencies}
            selectTicker={ticker => this.onTickerSelected(ticker)}
          />
        </CardText>
        <CardActions style={{ textAlign: 'right' }}>
          <FlatButton
            className="add-ticker-button"
            label="Add ticker"
            primary={true}
            onClick={event => this.onAddButtonClicked(event)}
            disabled={this.props.tickers.length === 0}
          />
        </CardActions>
      </Card>
    )
  }
}
