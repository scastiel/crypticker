import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { CardText } from 'material-ui/Card'
import muiThemeable from 'material-ui/styles/muiThemeable'

export default muiThemeable()(
  class Ticker extends Component {
    static propTypes = {
      ticker: PropTypes.shape({
        id: PropTypes.string.isRequired,
        currFrom: PropTypes.string.isRequired,
        currTo: PropTypes.string.isRequired,
        value: PropTypes.number
      }),
      currencies: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          symbol: PropTypes.string.isRequired
        })
      ),
      editMode: PropTypes.bool
    }
    getCurrency(currencyId) {
      return this.props.currencies.find(currency => currency.id === currencyId)
    }
    render() {
      const tickerStyle = {
        fontSize: 'large',
        textAlign: 'right'
      }

      const valueColor = this.props.muiTheme.palette.textColor
      const lightColor = this.props.muiTheme.palette.accent3Color

      const tickerElementStyle = { marginLeft: '10px', color: lightColor }

      return (
        <CardText className="ticker" style={tickerStyle}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'baseline'
            }}
          >
            <div
              className="one"
              style={{ ...tickerElementStyle, fontSize: 'x-large' }}
            >
              1
            </div>
            <div className="currency-from-symbol" style={tickerElementStyle}>
              {this.getCurrency(this.props.ticker.currFrom).symbol}
            </div>
            <div className="equal" style={tickerElementStyle}>=</div>
            <div
              className="ticker-value"
              style={{
                ...tickerElementStyle,
                color: valueColor,
                fontSize: 'x-large'
              }}
            >
              {typeof this.props.ticker.value === 'number'
                ? this.props.ticker.value.toLocaleString(
                    window.navigator.language,
                    { minimumSignificantDigits: 3, maximumSignificantDigits: 3 }
                  )
                : '…'}
            </div>
            <div className="currency-to-symbol" style={tickerElementStyle}>
              {this.getCurrency(this.props.ticker.currTo).symbol}
            </div>
          </div>
        </CardText>
      )
    }
  }
)
