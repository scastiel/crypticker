import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'
import { expect } from 'chai'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

const mockComponent = Component
jest.mock(
  '../Ticker/Ticker',
  () =>
    class TickerMock extends mockComponent {
      render () {
        return (
          <div className='ticker' data-edit-mode={this.props.editMode}>
            {this.props.ticker.id}
          </div>
        )
      }
    }
)
jest.mock(
  '../ListView/ListView',
  () =>
    class ListViewMock extends mockComponent {
      moveUp (event) {
        event.preventDefault()
        this.props.moveUpItem && this.props.moveUpItem(1)
      }
      moveDown (event) {
        event.preventDefault()
        this.props.moveDownItem && this.props.moveDownItem(1)
      }
      remove (event) {
        event.preventDefault()
        this.props.removeItem && this.props.removeItem(1)
      }
      render () {
        return (
          <div>
            <ul>
              {this.props.items.map((item, index) =>
                <li key={index}>{item}</li>
              )}
            </ul>
            <span className='edit-mode'>
              {this.props.editMode ? 'on' : 'off'}
            </span>
            <button className='move-up-button' onClick={e => this.moveUp(e)}>
              Up
            </button>
            <button
              className='move-down-button'
              onClick={e => this.moveDown(e)}
            >
              Down
            </button>
            <button className='remove-button' onClick={e => this.remove(e)}>
              Remove
            </button>
          </div>
        )
      }
    }
)
const TickerListView = require('./TickerListView').default

const currencies = [
  { id: 'BTC', symbol: 'Ƀ' },
  { id: 'EUR', symbol: '€' },
  { id: 'USD', symbol: '$' }
]
const tickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  },
  {
    id: 'BTCUSD',
    currFrom: 'BTC',
    currTo: 'USD',
    value: 1000.0
  }
]

it('should display tickers', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerListView tickers={tickers} currencies={currencies} />
    </MuiThemeProvider>,
    div
  )
  const tickerElements = div.querySelectorAll('.ticker')
  expect(Array.from(tickerElements).map(e => e.innerHTML)).to.deep.equal(
    tickers.map(t => t.id)
  )
  expect(
    Array.from(tickerElements).map(e => !!e.getAttribute('data-edit-mode'))
  ).to.deep.equal(tickers.map(() => false))
})

it('should show list view in edit mode if edit mode is on', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerListView
        editMode={true}
        tickers={tickers}
        currencies={currencies}
      />
    </MuiThemeProvider>,
    div
  )
  expect(div.querySelector('.edit-mode').innerHTML).to.equal('on')
  const tickerElements = div.querySelectorAll('.ticker')
  expect(
    Array.from(tickerElements).map(e => !!e.getAttribute('data-edit-mode'))
  ).to.deep.equal(tickers.map(() => true))
})

it('should not show list view in edit mode if edit mode is off', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerListView
        editMode={false}
        tickers={tickers}
        currencies={currencies}
      />
    </MuiThemeProvider>,
    div
  )
  expect(div.querySelector('.edit-mode').innerHTML).to.equal('off')
})

it('should respond to tickers actions', () => {
  const moveUpTicker = jest.fn()
  const moveDownTicker = jest.fn()
  const removeTicker = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerListView
      tickers={tickers}
      currencies={currencies}
      moveUpTicker={moveUpTicker}
      moveDownTicker={moveDownTicker}
      removeTicker={removeTicker}
    />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.move-up-button'))
  expect(moveUpTicker.mock.calls).to.have.lengthOf(1)
  expect(moveUpTicker.mock.calls[0][0]).to.equal(tickers[1])
  ReactTestUtils.Simulate.click(div.querySelector('.move-down-button'))
  expect(moveDownTicker.mock.calls).to.have.lengthOf(1)
  expect(moveDownTicker.mock.calls[0][0]).to.equal(tickers[1])
  ReactTestUtils.Simulate.click(div.querySelector('.remove-button'))
  expect(removeTicker.mock.calls).to.have.lengthOf(1)
  expect(removeTicker.mock.calls[0][0]).to.equal(tickers[1])
})

it('should not crash if no functions specified for ticker actions', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <TickerListView tickers={tickers} currencies={currencies} />,
    div
  )
  ReactTestUtils.Simulate.click(div.querySelector('.move-up-button'))
  ReactTestUtils.Simulate.click(div.querySelector('.move-down-button'))
  ReactTestUtils.Simulate.click(div.querySelector('.remove-button'))
})
