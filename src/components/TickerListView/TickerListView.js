import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ListView from '../ListView/ListView'
import Ticker from '../Ticker/Ticker'

export default class TickerListView extends Component {
  static propTypes = {
    editMode: PropTypes.bool,
    tickers: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        currFrom: PropTypes.string.isRequired,
        currTo: PropTypes.string.isRequired
      })
    ).isRequired,
    currencies: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        symbol: PropTypes.string.isRequired
      })
    ).isRequired,
    removeTicker: PropTypes.func,
    moveUpTicker: PropTypes.func,
    moveDownTicker: PropTypes.func
  }
  createItemFromTicker (ticker) {
    return (
      <Ticker
        ticker={ticker}
        currencies={this.props.currencies}
        editMode={this.props.editMode}
      />
    )
  }
  moveUpItem (index) {
    if (this.props.moveUpTicker) {
      this.props.moveUpTicker(this.props.tickers[index])
    }
  }
  moveDownItem (index) {
    if (this.props.moveDownTicker) {
      this.props.moveDownTicker(this.props.tickers[index])
    }
  }
  removeItem (index) {
    if (this.props.removeTicker) {
      this.props.removeTicker(this.props.tickers[index])
    }
  }
  render () {
    return (
      <ListView
        editMode={this.props.editMode}
        items={this.props.tickers.map(ticker =>
          this.createItemFromTicker(ticker)
        )}
        moveUpItem={index => this.moveUpItem(index)}
        moveDownItem={index => this.moveDownItem(index)}
        removeItem={index => this.removeItem(index)}
      />
    )
  }
}
