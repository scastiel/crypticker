# Crypticker

[**Crypticker**](http://scastiel.gitlab.io/crypticker/) is a minimalistic webapp to check current values of Bitcoin and Ether.

## Features

* available tickers: BTCEUR, BTCUSD, ETHEUR, ETHUSD, ETHBTC.
* data is fetched from [Kraken public API](https://www.kraken.com/help/api).
* optimized for iPhone (you can add the app to your home screen) but works well with other mobile platforms too.

## Contribute

If you like web development and want to contribute adding features, known that this project uses [React](https://facebook.github.io/react/) (with [create-react-app](https://github.com/facebookincubator/create-react-app)) and is fully tested; therefor it's an ideal project to contribute even if you're a beginner :)

You can also request features or report a problem by [creating a new issue](https://gitlab.com/scastiel/crypticker/issues).

## License

Published under the GNU GPL v3 (see [LICENSE](https://gitlab.com/scastiel/crypticker/blob/master/LICENSE)).